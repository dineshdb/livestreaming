# Adaptive Livestreaming

## Protocols
- Apple HLS
- [MPEG-DASH](https://en.wikipedia.org/wiki/Dynamic_Adaptive_Streaming_over_HTTP)
- Microsoft Smooth Steaming
- Adobe HDS
- RTMP
- RTSP
- RTP
- Exoplayer
- Shaka Player
- https://github.com/ossrs/srs
- WebRTC (wowza and the likes)
- https://gpac.wp.imt.fr/mp4box/

## MPEG Dash
- Codec agnostic
- International Standard

## Tools
- ffmpeg
- https://github.com/mediagoom and node-player (no livestreaming)
- https://developer.mozilla.org/en-US/docs/Web/HTML/DASH_Adaptive_Streaming_for_HTML_5_Video
- https://gstreamer.freedesktop.org/projects/dashwebm.html

## Getting started
- Run server
  * Initialize and build run the app using ``docker-compose up``.
  * While previous command is running, run ``docker exec -it <containerid> python manage.py createsuperuser`` in another terminal after identifying the authorization container.
  * Goto ``http://localhost:8000/admin/``, login, create a user and a __stream-key__ for the user.
- Start streaming with rtmp using __stream-key__ created in previous step. You can push video using OBStudio or ffmpeg.
  * Stream to ``rtmp://localhost:1935/live/`` and set the __stream-key__ in OBStudio and start streaming.
  * Or ``ffmpeg -i file.mkv -f flv rtmp://localhost:1935/live/<stream-key>``
- Play the stream using hls or rtmp.
  * RTMP: ``ffplay rtmp://localhost:1935/live/<stream-key>``. Will be disabled in production.
  * Or RTMP: ``ffplay rtmp://localhost:1935/hls/<stream-key>``. Will be disabled in production.
  * Or HTTP: ``ffplay http://localhost:8080/live/<stream-key>/index.m3u8``. Enabled in production. Stream key will be username in production. This does not work till the date of writing this.

## Next features
- Multirate encoding. Issue with dumping of file for hls and dash consumption.

## References
- https://opensource.com/article/19/1/basic-live-video-streaming-server
- https://github.com/arut/nginx-rtmp-module
- https://github.com/brocaar/nginx-rtmp-dockerfile
- https://docs.peer5.com/guides/setting-up-hls-live-streaming-server-using-nginx/
- https://benwilber.github.io/nginx/rtmp/live/video/streaming/2018/03/25/building-a-live-video-streaming-website-part-1-start-streaming.html

## License
MIT License

## Copyright
- [Dinesh Bhattarai](https://dbhattarai.info.np)
