user www-data;
worker_processes 1;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
	worker_connections 1024;
	# multi_accept on;
}

rtmp {
	server {
		listen 1935;
		chunk_size 4096;
		
		application live {
            live on;

            # Don't allow RTMP playback
#            deny play all;
            push rtmp://localhost:1935/hls;

            # The on_publish callback will redirect the RTMP stream to the 
            # streamer's username, rather than their secret stream key.
            on_publish http://auth:8000/start_stream;
            on_publish_done http://auth:8000/stop_stream;
        }
        
       
        # This has not been tested.
        application encoder {
        	live on;
            exec ffmpeg -i rtmp://localhost:1935/encoder/$name
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 2500k -f flv -g 30 -r 30 -s 1280x720 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_720p2628kbs
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 1000k -f flv -g 30 -r 30 -s 854x480 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_480p1128kbs
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 750k -f flv -g 30 -r 30 -s 640x360 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_360p878kbs
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 400k -f flv -g 30 -r 30 -s 426x240 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_240p528kbs
              -c:a libfdk_aac -b:a 64k -c:v libx264 -b:v 200k -f flv -g 15 -r 15 -s 426x240 -preset superfast -profile:v baseline rtmp://localhost:1935/hls/$name_240p264kbs;
        }

        application hls {
            live on;

            # Only accept publishing from localhost.
            # (the `app` RTMP ingest application)
#            deny play all;

            # Package streams as HLS
            hls on;
            hls_fragment 3;
            hls_playlist_length 60;
            hls_path /var/www/live;
            hls_nested on; # create a new folder for each stream
            hls_fragment_naming system;

			hls_variant _low BANDWIDTH=288000; # Low bitrate, sub-SD resolution
	        hls_variant _mid BANDWIDTH=448000; # Medium bitrate, SD resolution
    	    hls_variant _high BANDWIDTH=1152000; # High bitrate, higher-than-SD resolution
    	    hls_variant _hd720 BANDWIDTH=2048000; # High bitrate, HD 720p resolution
    	    hls_variant _src BANDWIDTH=4096000; # Source bitrate, source resolution
    	    
    	    dash on;
			dash_path /var/www/dash;
			dash_fragment 15s;

            
            record_notify on;
            record_path /var/www/videos;
            record all;
            record_unique on;

			exec ffmpeg -i SOURCE \
              -c:a libfdk_aac -b:a 32k  -c:v libx264 -b:v 128K -preset superfast -f flv rtmp://localhost/hls/$name_low \
              -c:a libfdk_aac -b:a 64k  -c:v libx264 -b:v 256k -preset superfast -f flv rtmp://localhost/hls/$name_mid \
              -c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 512K -preset superfast -f flv rtmp://localhost/hls/$name_hi;
        }
    }
}

http {
    log_format main '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for" $request_time';
    sendfile off;
	directio 512;
    tcp_nopush on;
    tcp_nodelay on;
    keepalive_timeout 65;
    types_hash_max_size 2048;

    include /etc/nginx/mime.types;
    default_type application/octet-stream;

	ssl_protocols TLSv1.1 TLSv1.2 TLSv1.3;
	ssl_prefer_server_ciphers on;

	access_log /var/log/nginx/access.log;
	error_log /var/log/nginx/error.log;

	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;

	upstream auth {
		server auth:8000;
	}
    server {
        listen 8080 default_server;
        server_name _;
        root /var/www/;
        expires -1d;

		location /live {
			add_header 'Cache-Control' 'no-cache';
			add_header 'Access-Control-Allow-Origin' '*' always;
	        add_header 'Access-Control-Expose-Headers' 'Content-Length,Content-Range';
    	    add_header 'Access-Control-Allow-Headers' 'Range';
	
			if ($request_method = 'OPTIONS') {
		        add_header 'Access-Control-Allow-Origin' '*';
		        add_header 'Access-Control-Allow-Headers' 'Range';
		        add_header 'Access-Control-Max-Age' 1728000;
		        add_header 'Content-Type' 'text/plain charset=UTF-8';
		        add_header 'Content-Length' 0;
		        return 204;
			}	
			types {
				application/dash+xml mpd;
				application/vnd.apple.mpegurl m3u8;
				video/mp2t ts;
			}
			
			root /var/www/live;
		
		}
		location /dash {
            root /var/www/dash;
            add_header Cache-Control no-cache;

            # To avoid issues with cross-domain HTTP requests (e.g. during development)
            add_header Access-Control-Allow-Origin *;
        }
    }
}
